'''
首页-官方充值
'''

# -*- coding: utf-8  -*-
__author__ = 'snake'


import time
import unittest
from src.bll.v344.login import one_key_login
from src.util.util_logger import logger
from src.bll.v344.index import back_to_index
from src.util.util_appium_driver import get_driver
from src.util.util_appium_tools import find_element
from src.util.util_appium_tools import find_elements


@unittest.skip("")
class TestCasegfcz(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("=" * 100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("=" * 100)

    def setUp(self):
        logger.info("*" * 100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))
        logger.info("开始执行返回首页")
        back_to_index(self.driver)

        if one_key_login(self.driver) is False:
            logger.info("判断掌厅登录状态失败，app启动异常!")
            raise Exception("判断掌厅登录状态失败，app启动异常!!")

    def tearDown(self):
        self.driver._switch_to.context(self.driver.contexts[0])
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))


    def test_gfcz_checknumber(self):
        '''
        充值号码应该与登录号码一致
        :return:
        '''
        # 获取用户登录的手机号码
        user_index_number = find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv")
        user_info_number = user_index_number.text

        # 获取用户充值页面号码
        guanfangchongzhi = find_element(self.driver,'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()
        user_chongzhi_index_number = find_element(self.driver,"id","com.sunrise.scmbhc:id/mPhoneNumber")
        user_chongzhi_number = user_chongzhi_index_number.text

        self.assertEquals(user_chongzhi_number,user_info_number,"检查用户号码与首页号码一致")


    def test_gfcz_checktext(self):
        '''
        检查官方充值，品质保障文本
        :return:
        '''
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        text_id = "com.sunrise.scmbhc:id/charge_num_hint"
        official_index = find_element(self.driver,"id",text_id)
        official_text = official_index.text
        self.assertEquals("官方充值 品质保障",official_text,"检查文字的一致性")


    def test_gfcz_checkzfb(self):
        '''
        检查支付方式中存在支付宝
        :return:
        '''
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        zhifubao_tubiao = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[1]"
        zhifubao_shortcut = find_element(self.driver,"xpath",zhifubao_tubiao)
        if zhifubao_shortcut is not None:
            self.assertEquals(True,True,"支付宝快捷方式存在")
        else:
            self.assertEquals(True,False,"支付宝快捷方式不存在")


    def test_gfcz_checkweixin(self):
       '''
       检查微信快捷方式存在
       :return:
       '''
       guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
       guanfangchongzhi.click()

       weixin_tubiao = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[2]"
       weixin_shortcut = find_element(self.driver, "xpath", weixin_tubiao)
       if weixin_shortcut is not None:
            self.assertEquals(True,True,"微信快捷方式存在")
       else:
            self.assertEquals(True,False,"微信快捷方式不存在")


    def test_gfcz_checkhebzh(self):
        '''
        检查和包支付快捷方式存在
        :return:
        '''
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        hebaozf_tubiao = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[3]"
        hebaozh_shortcut = find_element(self.driver, "xpath", hebaozf_tubiao)
        if hebaozh_shortcut is not None:
            self.assertEquals(True, True, "和包支付快捷方式存在")
        else:
            self.assertEquals(True, False, "和包支付快捷方式不存在")


    def test_gfcz_checklinks(self):
        '''
        检查交费记录查询链接
        :return:
        '''
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        #跳转到交费记录查询页面
        jfjl = "//android.widget.ListView[@resource-id='com.sunrise.scmbhc:id/charge_listview']/android.widget.LinearLayout[1]"
        find_element(self.driver,"xpath",jfjl).click()

        #检查提示语是否正确，正确则证明跳转正常
        pageone = "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/recharge_record_tips']"
        pageoneifexist = find_element(self.driver,"xpath",pageone)
        if pageoneifexist is not None:
            self.assertEquals(1,1,"交费记录页面跳转后页面正常显示")
        else:
            self.assertEquals(1,2,"交费记录页面不存在或者交费记录异常")


    @unittest.skip("")
    def test_czjf_checkhuodxize(self):
        '''
        检查和包95折活动细则
        :return:
        '''
        #跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver,'xpath',"//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()
        time.sleep(1)

        #跳转到活动细则页面
        self.driver.swipe(100,800,100,100)
        obj = find_elements(self.driver,"id","com.sunrise.scmbhc:id/rl_rule_tips")
        # logger.info(len(obj))
        # obj[1].click()
        #
        # czjf = "//android.view.View[@text='30元']"
        # jfimage = "//android.view.View[@text='300元']"
        # title = find_element(self.driver, "xpath", czjf)
        # image = find_element(self.driver, "xpath", jfimage)
        # if title and image is not None:
        #     self.assertEquals(1, 1, "和包95折活动细则跳转过去页面正常")
        # else:
        #     self.assertEquals(1, 2, "和包95折活动细则页面异常")



    def test_gfcz_zhifubao10jine(self):
        '''
        检查支付宝支付10元的显示
        :return:
        '''
        # 跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        #选择支付方式和充值金额
        zhbshortcut = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[1]"
        clickshortcut = find_element(self.driver,"xpath",zhbshortcut)
        clickshortcut.click()
        time.sleep(1)

        zhifujine = "//android.widget.TextView[@text='10元支付9.98元']"
        zhifujineid = find_element(self.driver,"id","com.sunrise.scmbhc:id/yh_gridview")
        objs = find_elements(zhifujineid,"id","com.sunrise.scmbhc:id/txt")

        # logger.info(len(objs))
        # objs[0].click()
        #
        # oopb = find_element(self.driver,"id","com.sunrise.scmbhc:id/money_num")
        # oopb1 = oopb.text
        # actual = find_element(self.driver,"id","com.sunrise.scmbhc:id/money_actual_num")
        # actualmoney = actual.text
        #
        # if oopb1 == "9.98" and actualmoney == "10":
        #     self.assertEquals(1,1,"支付宝支付10元显示正确")
        # else:
        #     self.assertEquals(1,2,"支付宝支付10元异常")


    def test_gfcz_zhifubaojine200jine(self):
        '''
        检查支付宝支付200元的界面显示
        :return:
        '''
        # 跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        # 选择支付方式和充值金额
        zhbshortcut = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[1]"
        clickshortcut = find_element(self.driver, "xpath", zhbshortcut)
        clickshortcut.click()
        time.sleep(1)

        zhifujineid = find_element(self.driver, "id", "com.sunrise.scmbhc:id/yh_gridview")
        objs = find_elements(zhifujineid, "id", "com.sunrise.scmbhc:id/txt")
        logger.info(len(objs))
        objs[4].click()

        zhifu = find_element(self.driver,"id","com.sunrise.scmbhc:id/money_num")
        zhifujine = zhifu.text
        actual = find_element(self.driver,"id","com.sunrise.scmbhc:id/money_actual_num")
        actualmoney = actual.text

        if zhifujine == "199.6" and actualmoney == "200":
            self.assertEquals(1,1,"支付宝支付200元显示正常")
        else:
            self.assertEquals(1,2,"支付宝支付200元异常")


    def test_gfcz_weixinzhif50(self):
        '''
        检查微信支付50元金额的显示
        :return:
        '''
        # 跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        #选择微信支付
        weixinshortcut = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[2]"
        clickshortcut = find_element(self.driver, "xpath", weixinshortcut)
        clickshortcut.click()
        time.sleep(1)
        #选择微信支付金额
        zhifujineid = find_element(self.driver, "id", "com.sunrise.scmbhc:id/yh_gridview")
        objs = find_elements(zhifujineid, "id", "com.sunrise.scmbhc:id/txt")
        logger.info(len(objs))
        objs[2].click()

        #判断支付金额和实际到账金额显示
        zhifu = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_num")
        zhifujine = zhifu.text
        actual = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_actual_num")
        actualmoney = actual.text

        if zhifujine == "49.9" and actualmoney == "50":
            self.assertEquals(1,1,"微信支付50元显示正常")
        else:
            self.assertEquals(1,2,"微信支付50元异常")


    def test_gfcz_weixinzhif100(self):
        '''
        检查微信支付100元的界面显示
        :return:
        '''
        # 跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        # 选择微信支付
        weixinshortcut = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[2]"
        clickshortcut = find_element(self.driver, "xpath", weixinshortcut)
        clickshortcut.click()
        time.sleep(1)

        # 选择微信支付金额
        zhifujineid = find_element(self.driver, "id", "com.sunrise.scmbhc:id/yh_gridview")
        objs = find_elements(zhifujineid, "id", "com.sunrise.scmbhc:id/txt")
        logger.info(len(objs))
        objs[3].click()

        # 判断支付金额和实际到账金额显示
        zhifu = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_num")
        zhifujine = zhifu.text
        actual = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_actual_num")
        actualmoney = actual.text

        if zhifujine == "99.8" and actualmoney == "100":
            self.assertEquals(1, 1, "微信支付100元显示正常")
        else:
            self.assertEquals(1, 2, "微信支付100元异常")


    def test_gfcz_hebaozhif30(self):
        '''
        检查和包充值30元的显示
        :return:
        '''
        # 跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        # 选择和包支付
        hebaoshortcut = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[3]"
        clickshortcut = find_element(self.driver, "xpath", hebaoshortcut)
        clickshortcut.click()
        time.sleep(1)

        #选择支付金额
        zhifujineid = find_element(self.driver, "id", "com.sunrise.scmbhc:id/yh_gridview")
        objs = find_elements(zhifujineid, "id", "com.sunrise.scmbhc:id/txt")
        logger.info(len(objs))
        objs[0].click()

        # 判断支付金额和实际到账金额显示
        zhifu = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_num")
        zhifujine = zhifu.text
        actual = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_actual_num")
        actualmoney = actual.text

        if zhifujine == "30" and actualmoney == "30+1.8":
            self.assertEquals(1, 1, "和包支付30元显示正常")
        else:
            self.assertEquals(1, 2, "和包支付30元异常")


    def test_gfcz_hebaozhif300(self):
        '''
        检查和包支付300元界面显示
        :return:
        '''
        # 跳转到官方充值页面
        guanfangchongzhi = find_element(self.driver, 'xpath', "//android.widget.TextView[@text='官方充值']")
        guanfangchongzhi.click()

        self.driver.swipe(100, 800, 100, 100)

        # 选择和包支付
        hebaoshortcut = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/gv_pay']/android.widget.LinearLayout[3]"
        clickshortcut = find_element(self.driver, "xpath", hebaoshortcut)
        clickshortcut.click()
        time.sleep(1)

        # 选择支付金额
        zhifujineid = find_element(self.driver, "id", "com.sunrise.scmbhc:id/yh_gridview")
        objs = find_elements(zhifujineid, "id", "com.sunrise.scmbhc:id/txt")
        logger.info(len(objs))
        objs[4].click()

        # 判断支付金额和实际到账金额显示
        zhifu = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_num")
        zhifujine = zhifu.text
        actual = find_element(self.driver, "id", "com.sunrise.scmbhc:id/money_actual_num")
        actualmoney = actual.text

        if zhifujine == "300" and actualmoney == "300+18":
            self.assertEquals(1, 1, "和包支付300元显示正常")
        else:
            self.assertEquals(1, 2, "和包支付300元异常")












































