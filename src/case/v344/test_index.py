"""
    首页
"""

# -*- coding: utf-8  -*-
__author__ = 'snake'

import unittest
from src.util.util_logger import logger
from src.bll.v344.login import one_key_login
from src.bll.v344.index import back_to_index
from src.util.util_appium_driver import get_driver
from src.util.util_appium_tools import find_element


class TestCaseIndex(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("="*100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("="*100)

    def setUp(self):
        logger.info("*"*100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))
        logger.info("开始执行返回首页")
        back_to_index(self.driver)

        if one_key_login(self.driver) is False:
            logger.info("判断掌厅登录状态失败，app启动异常!")
            raise Exception("判断掌厅登录状态失败，app启动异常!!")

    def tearDown(self):
        self.driver._switch_to.context(self.driver.contexts[0])
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def test_hkzq(self):
        """
        号卡专区
        :return:
        """
        hkzq = "//android.widget.TextView[@text='号卡专区']"
        obj = find_element(self.driver, "xpath", hkzq)
        if obj is not None:
            self.assertEquals(True, True, "号码专区入口存在")
        else:
            self.assertEquals(True, False, "未找到号码专区入口")

    def test_yjcx(self):
        """
        一键查询
        :return:
        """
        yjcx = "//android.widget.TextView[@text='一键查询']"
        obj = find_element(self.driver, "xpath", yjcx)
        if obj is not None:
            self.assertEquals(True, True, "一键查询入口存在")
        else:
            self.assertEquals(True, False, "未找到一键查询入口")

    def test_gfcz(self):
        """
        官方充值
        :return:
        """
        gfcz = "//android.widget.TextView[@text='官方充值']"
        obj = find_element(self.driver, "xpath", gfcz)
        if obj is not None:
            self.assertEquals(True, True, "官方充值入口存在")
        else:
            self.assertEquals(True, False, "未找到官方充值入口")

    def test_ywbl(self):
        """
        业务办理
        :return:
        """
        ywbl = "//android.widget.TextView[@text='业务办理']"
        obj = find_element(self.driver, "xpath", ywbl)
        if obj is not None:
            self.assertEquals(True, True, "业务办理入口存在")
        else:
            self.assertEquals(True, False, "未找到业务办理入口")

    def test_cdth(self):
        """
        成都特惠
        :return:
        """
        cdth = "//android.widget.TextView[@text='成都特惠']"
        obj = find_element(self.driver, "xpath", cdth)
        if obj is not None:
            self.assertEquals(True, True, "成都特惠入口存在")
        else:
            self.assertEquals(True, False, "未找到成都特惠入口")

    def test_xjtq(self):
        """
        星级特权
        :return:
        """
        xjtq = "//android.widget.TextView[@text='星级特权']"
        obj = find_element(self.driver, "xpath", xjtq)
        if obj is not None:
            self.assertEquals(True, True, "星级特权入口存在")
        else:
            self.assertEquals(True, False, "未找到星级特权入口")

    def test_rmyl(self):
        """
        热门娱乐
        :return:
        """
        rmyl = "//android.widget.TextView[@text='热门娱乐']"
        obj = find_element(self.driver, "xpath", rmyl)
        if obj is not None:
            self.assertEquals(True, True, "热门娱乐入口存在")
        else:
            self.assertEquals(True, False, "未找到娱乐入口入口")

    def test_gdfw(self):
        """
        更多服务
        :return:
        """
        gdfw = "//android.widget.TextView[@text='更多服务']"
        obj = find_element(self.driver, "xpath", gdfw)
        if obj is not None:
            self.assertEquals(True, True, "更多服务入口存在")
        else:
            self.assertEquals(True, False, "未找到更多服务入口")

    def test_moreservices_dzfp(self):
        """
        更多服务——电子发票
        :return:
        """
        dzfp = "//android.widget.TextView[@text='电子发票']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位电子发票按钮, by->xpath, value->{}".format(dzfp))
        obj = find_element(self.driver, "xpath", dzfp)
        if obj is not None:
            self.assertEquals(True, True, "电子发票入口存在")
        else:
            self.assertEquals(True, False, "未找到电子发票入口")

    def test_moreservices_zxkf(self):
        """
        更多服务——在线客服
        :return:
        """
        zxkf = "//android.widget.TextView[@text='在线客服']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位在线客服按钮, by->xpath, value->{}".format(zxkf))
        obj = find_element(self.driver, "xpath", zxkf)

        if obj is not None:
            self.assertEquals(True, True, "在线客服入口存在")
        else:
            self.assertEquals(True, False, "未找到在线客服入口")

    def test_moreservices_xkjh(self):
        """
        更多服务——新卡激活
        :return:
        """
        xkjh = "//android.widget.TextView[@text='新卡激活']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始定位更多服务按钮并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位新卡激活按钮, by->xpath, value->{}".format(xkjh))
        obj = find_element(self.driver, "xpath", xkjh)

        if obj is not None:
            self.assertEquals(True, True, "新卡激活入口存在")
        else:
            self.assertEquals(True, False, "未找到新卡激活入口")

    def test_moreservices_xdcx(self):
        """
        更多服务——详单查询
        :return:
        """
        xdcx = "//android.widget.TextView[@text='详单查询']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位详单查询按钮, by->xpath, value->{}".format(xdcx))
        obj = find_element(self.driver, "xpath", xdcx)

        if obj is not None:
            self.assertEquals(True, True, "详单查询入口存在")
        else:
            self.assertEquals(True, False, "未找到详单查询入口")

    def test_moreservices_tclc(self):
        """
        更多服务——套餐理财
        :return:
        """
        tclc = "//android.widget.TextView[@text='套餐理财']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位套餐理财按钮, by->xpath, value->{}".format(tclc))
        obj = find_element(self.driver, "xpath", tclc)

        if obj is not None:
            self.assertEquals(True, True, "套餐理财入口存在")
        else:
            self.assertEquals(True, False, "未找到套餐理财入口")

    def test_moreservices_fwpj(self):
        """
        更多服务——服务评价
        :return:
        """
        fwpj = "//android.widget.TextView[@text='服务评价']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位服务评价按钮, by->xpath, value->{}".format(fwpj))
        obj = find_element(self.driver, "xpath", fwpj)

        if obj is not None:
            self.assertEquals(True, True, "服务评价入口存在")
        else:
            self.assertEquals(True, False, "未找到服务评价入口")

    def test_moreservices_zdcx(self):
        """
        更多服务——账单查询
        :return:
        """
        zdcx = "//android.widget.TextView[@text='账单查询']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位账单查询按钮, by->xpath, value->{}".format(zdcx))
        obj = find_element(self.driver, "xpath", zdcx)

        if obj is not None:
            self.assertEquals(True, True, "账单查询入口存在")
        else:
            self.assertEquals(True, False, "未找到账单查询入口")

    def test_moreservices_kdyy(self):
        """
        更多服务——宽带预约
        :return:
        """
        kdyy = "//android.widget.TextView[@text='宽带预约']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位宽带预约按钮, by->xpath, value->{}".format(kdyy))
        obj = find_element(self.driver, "xpath", kdyy)

        if obj is not None:
            self.assertEquals(True, True, "宽带预约入口存在")
        else:
            self.assertEquals(True, False, "未找到宽带预约入口")

    def test_moreservices_jfdh(self):
        """
        更多服务——积分兑换
        :return:
        """
        jfdh = "//android.widget.TextView[@text='积分兑换']"
        gdfw = "//android.widget.TextView[@text='更多服务']"

        logger.info("开始查找更多服务并点击, by->xpath, value->{}".format(gdfw))
        # gdfw_obj = find_element(self.driver, "xpath", gdfw)
        f_gdfw_obj = find_element(self.driver, "id", "com.sunrise.scmbhc:id/bis_gridview")
        gdfw_obj = find_element(f_gdfw_obj, "xpath", gdfw)
        self.assertNotEqual(gdfw_obj, None, "更多服务按钮不能为空")
        gdfw_obj.click()

        logger.info("点击按钮完成，进入更多服务页面定位积分兑换按钮, by->xpath, value->{}".format(jfdh))
        obj = find_element(self.driver, "xpath", jfdh)

        if obj is not None:
            self.assertEquals(True, True, "积分兑换入口存在")
        else:
            self.assertEquals(True, False, "未找到积分兑换入口")

    def test_rmhd(self):
        """
        流量专区--热门活动
        :return:
        """
        xpath = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/iv_amuse_two']/android.widget.ImageView[1]"

        # 判断热门活动入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "热门活动入口存在")
        else:
            self.assertEquals(True, False, "未找到热门活动入口")

    def test_nzxl(self):
        """
        流量专区--年中献礼
        :return:
        """
        xpath = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/iv_amuse_three']/android.widget.ImageView[1]"

        # 判断年中献礼入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "年中献礼入口存在")
        else:
            self.assertEquals(True, False, "未找到年中献礼入口")

    def test_cnxh(self):
        """
        流量专区--猜你喜欢
        :return:
        """
        xpath = "//android.widget.ImageView[@resource-id='com.sunrise.scmbhc:id" \
                "/img_love']"

        # 判断猜你喜欢入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "猜你喜欢入口存在")
        else:
            self.assertEquals(True, False, "未找到猜你喜欢入口")

    def test_llzq_spllb(self):
        """
        流量专区--视频王流量包
        :return:
        """
        xpath = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/market_layout1']/android.widget.ImageView[1]"

        # 判断视频流量包入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "视频王流量包入口存在")
        else:
            self.assertEquals(True, False, "未找到视频王流量包入口")

    def test_llzq_yyllb(self):
        """
        流量专区--音乐流量包
        :return:
        """
        xpath = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/market_layout2']/android.widget.ImageView[1]"

        # 判断音乐流量包入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "音乐流量包入口存在")
        else:
            self.assertEquals(True, False, "未找到音乐流量包入口")

    def test_llzq_lljyz(self):
        """
        流量专区--流量加油站
        :return:
        """
        xpath = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/market_layout21']/android.widget.ImageView[1]"

        # 判断流量加油站入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "流量加油站入口存在")
        else:
            self.assertEquals(True, False, "未找到流量加油站入口")

    def test_llzq_bxljsb(self):
        """
        流量专区--不限量加速包
        :return:
        """
        xpath = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/market_layout22']/android.widget.ImageView[1]"

        # 判断不限量加速包入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "不限量加速包入口存在")
        else:
            self.assertEquals(True, False, "未找到不限量加速包入口")

    def test_shouye_qdtb(self):
        """
        首页签到图标
        :return:
        """
        xpath = "//android.widget.RelativeLayout[@resource-id='com.sunrise.scmbhc:id" \
                "/sildingFinishLayout']/android.widget.ImageView[1]"

        # 判断首页签到图标入口是否存在
        chess = find_element(self.driver, "xpath", xpath)
        if chess is not None:
            self.assertEquals(True, True, "首页签到图标入口存在")
        else:
            self.assertEquals(True, False, "未找到首页签到图标入口")

    def test_kdzq_kdfw(self):
        """
        首页宽带专区-宽带服务
        :return:
        """
        self.driver.swipe(500, 800, 500, 200)

        f_kdzq = "com.sunrise.scmbhc:id/root_broadband_yh"
        kdfw = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id/broad_layout1']/android.widget.ImageView[1]"
        f_kdzq_obj = find_element(self.driver, "id", f_kdzq)
        kdfw_obj = find_element(f_kdzq_obj, "xpath", kdfw)

        self.assertNotEqual(kdfw_obj, None, "宽带服务入口")

        # 判断首页宽带服务入口是否存在
        chess = find_element(self.driver, "xpath", kdfw)
        if chess is not None:
            self.assertEquals(True, True, "首页宽带服务入口存在")
        else:
            self.assertEquals(True, False, "未找到首页宽带服务入口")

    def test_kdzq_yyxz(self):
        """
        首页宽带专区-预约新装
        :return:
        """
        self.driver.swipe(500, 800, 500, 200)

        f_kdzq = "com.sunrise.scmbhc:id/root_broadband_yh"
        yyxz = "//android.widget.ImageView[@resource-id='com.sunrise.scmbhc:id/market_img2']"
        f_kdzq_obj = find_element(self.driver, "id", f_kdzq)
        yyxz_obj = find_element(f_kdzq_obj, "xpath", yyxz)

        self.assertNotEqual(yyxz_obj, None, "预约新装入口")

        # 判断首页预约新装入口是否存在
        chess = find_element(self.driver, "xpath", yyxz)
        if chess is not None:
            self.assertEquals(True, True, "首页预约新装入口存在")
        else:
            self.assertEquals(True, False, "未找到首页预约新装入口")

    def test_kdzq_zhjt(self):
        """
        首页宽带专区-智慧家庭
        :return:
        """
        self.driver.swipe(500, 800, 500, 200)

        f_kdzq = "com.sunrise.scmbhc:id/root_broadband_yh"
        zhjt = "//android.widget.LinearLayout[@resource-id='com.sunrise.scmbhc:id/broad_layout3']/android.widget.ImageView[1]"
        f_kdzq_obj = find_element(self.driver, "id", f_kdzq)
        zhjt_obj = find_element(f_kdzq_obj, "xpath", zhjt)

        self.assertNotEqual(zhjt_obj, None, "智慧家庭入口")

        # 判断首页智慧家庭入口是否存在
        chess = find_element(self.driver, "xpath", zhjt)
        if chess is not None:
            self.assertEquals(True, True, "首页智慧家庭入口存在")
        else:
            self.assertEquals(True, False, "未找到首页智慧家庭入口")

    @unittest.skip("method")
    def test_xj_yjcx(self):
        """
        检查星级显示，规则：星级为准星用户、一星用户、二星用户、三星用户、四星用户、五星用户
        :return:
        """

        #检查一键查询入口是否存在
        yjcx = "//android.widget.GridView[@resource-id='com.sunrise.scmbhc:id/bis_gridview']/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]"
        obj_yjcx = find_element(self.driver,'xpath',yjcx)
        self.assertNotEqual(obj_yjcx, None, "判断一键查询入口存在")
        obj_yjcx.click()

        #判断星级对象父元素是否存在
        f_star = "//android.webkit.WebView[@content-desc='消费查询']/android.view.View[1]/android.view.View[1]"
        obj_f_star = find_element(self.driver,"xpath",f_star)
        self.assertEquals(obj_f_star,None,"判断星级父元素存在")

        # 信用星级对象
        user_star = obj_f_star.find_element_by_class_name("android.view.View")[1].text

        # 不为空
        if user_star is None or user_star == "":
            self.assertEquals(1, 2, "我的-信用星级为空")

        # 在等级范围内
        if user_star not in ["准星用户", "一星用户", "二星用户", "三星用户", "四星用户", "五星用户", "无星级"]:
            msg = "我的-信用星级异常,信用星级-{}".format(user_star)
            self.assertEquals(1, 2, msg)

