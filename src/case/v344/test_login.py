# -*- coding: utf-8  -*-
__author__ = 'snake'

import unittest
from src.util.util_logger import logger
from src.bll.v344.login import one_key_login
from src.util.util_appium_driver import get_driver


@unittest.skip("Skipping this class")
class TestCaseLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("="*100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("="*100)

    def setUp(self):
        logger.info("*"*100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def tearDown(self):
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def test_login_success(self):
        # self.assertEquals(logout(self.driver), True, "测试退出账号")
        self.assertEquals(one_key_login(self.driver), True, "测试一键登录成功")
        # self.assertEquals(user_phone_login(self.driver), True, "测试手机号短信码登录成功")

    @unittest.skip("Skipping this test method")
    def test_login_failed(self):
        # self.assertEquals(logout(self.driver), True, "测试退出账号")
        self.assertEquals(one_key_login(self.driver), False, "测试一键登录失败")
        # self.assertEquals(user_phone_login(self.driver), False, "测试手机号短信码登录失败")
