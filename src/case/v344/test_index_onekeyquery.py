"""
    首页-一键查询
"""

# -*- coding: utf-8  -*-
__author__ = 'snake'


import unittest
from src.bll.v344.login import one_key_login
from src.util.util_logger import logger
from src.bll.v344.index import back_to_index
from src.util.util_appium_driver import get_driver
from src.util.util_appium_tools import find_element


@unittest.skip("")
class TestCaseMy(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("="*100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("="*100)

    def setUp(self):
        logger.info("*"*100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))
        logger.info("开始执行返回首页")
        back_to_index(self.driver)

        if one_key_login(self.driver) is False:
            logger.info("判断掌厅登录状态失败，app启动异常!")
            raise Exception("判断掌厅登录状态失败，app启动异常!!")

    def tearDown(self):
        self.driver._switch_to.context(self.driver.contexts[0])
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def test_yjcx_kyjf(self):
        """
        可用积分：积分不能为空
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言可用积分
        score_xpath = "/html/body/div[1]/div[1]/div[2]/p"
        score_obj = find_element(self.driver, "xpath", score_xpath)

        logger.info("可用积分为->{}".format(score_obj.text))
        self.assertNotEqual(score_obj.text, None, "可用积分不为空")

    def test_yjcx_dyye(self):
        """
        当月余额：余额不能为空
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言当月余额
        score_xpath = "/html/body/div[1]/div[2]/div[1]/div[1]/span"
        score_obj = find_element(self.driver, "xpath", score_xpath)

        logger.info("当月余额为->{}".format(score_obj.text))
        self.assertNotEqual(score_obj.text, None, "当月余额不为空")
        self.assertNotEqual(score_obj.text, "", "当月余额不为空")

    def test_yjcx_dyxf(self):
        """
        当月消费：消费不能为空
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言当月消费
        score_xpath = "/html/body/div[1]/div[2]/div[2]/div[1]/span"
        score_obj = find_element(self.driver, "xpath", score_xpath)

        logger.info("当月消费为->{}".format(score_obj.text))
        self.assertNotEqual(score_obj.text, None, "当月消费不为空")
        self.assertNotEqual(score_obj.text, "", "当月消费不为空")

    def test_yjcx_syye(self):
        """
        剩余流量：剩余流量不能为空
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言剩余流量
        score_xpath = "/html/body/div[2]/div[1]/ul/li[1]/a/div[2]/span"
        score_obj = find_element(self.driver, "xpath", score_xpath)

        logger.info("剩余流量为->{}".format(score_obj.text))
        self.assertNotEqual(score_obj.text, None, "剩余流量不为空")
        self.assertNotEqual(score_obj.text, "", "剩余流量不为空")

    def test_yjcx_syyy(self):
        """
        剩余语音：剩余语音不能为空
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言剩余语音
        score_xpath = "/html/body/div[2]/div[1]/ul/li[2]/a/div[2]/span"
        score_obj = find_element(self.driver, "xpath", score_xpath)

        logger.info("剩余语音为->{}".format(score_obj.text))
        self.assertNotEqual(score_obj.text, None, "剩余语音不为空")
        self.assertNotEqual(score_obj.text, "", "剩余语音不为空")

    def test_yjcx_lljyz(self):
        """
        流量加油站:入口存在既可
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言流量加油站
        score_xpath = "/html/body/div[4]/ul/li[1]/a/img"
        score_obj = find_element(self.driver, "xpath", score_xpath)
        self.assertNotEqual(score_obj, None, "流量加油站入口存在")

    def test_yjcx_dwk(self):
        """
        大王卡：入口存在既可
        :return:
        """
        # 一键查询
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        # 切换到webview
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        # 定位并断言大王卡
        score_xpath = "/html/body/div[4]/ul/li[2]/a/img"
        score_obj = find_element(self.driver, "xpath", score_xpath)
        self.assertNotEqual(score_obj, None, "大王卡入口存在")