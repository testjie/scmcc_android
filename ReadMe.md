### scmcc_android
    基于appium-python的安卓app自动化测试框架

#### 1. 项目简介
    四川移动掌厅安卓端自动化测试框架,用于掌厅自动巡检/回归测试

#### 2. 环境配置
    Windwos 10 64bit + Python36
    参照 ~/libs/Readme.md

#### 3. 如何使用
    持续集成或直接运行python run.py

#### 4. 问题解决
    ps:如果使用自定义的appium,
    在首次安装完unlock.apk, appium setting.apk, android input manager.apk后
    建议做以下操作屏蔽各种国（la）产（ji）ROM的弹窗
    
    1. 禁止appium重复安装Unlock、Setting、Android Input Manager等apk，防止第三方rom出现弹窗提示的问题
    
    step1:
    ~\Appium\resources\app\node_modules\appium\node_modules\appium-android-driver\lib\android-helpers.js
    搜索并注释:
    await adb.install(unicodeIMEPath, {replace: false})
    await helpers.pushSettingsApp(adb)
    await helpers.pushUnlock(adb)
    
    step2:
    \Appium\resources\app\node_modules\appium\node_modules\appium-android-driver\build\lib\android-helpers.js
    a. 
    注释这行：
    return _regeneratorRuntime.awrap(adb.install(_appiumAndroidIme.path, { replace: false }));
    替换成这行：
    return context$1$0.abrupt('return', false);
    
    b. 
    注释这行：
    return _regeneratorRuntime.awrap(helpers.pushSettingsApp(adb));
    替换成这行：
    return context$1$0.abrupt('return', false);
    
    c. 
    注释这行：
    return _regeneratorRuntime.awrap(helpers.pushUnlock(adb));
    替换成这行：
    return context$1$0.abrupt('return', false);

    2. driver.close() 出现报错，待解决
    selenium.common.exceptions.WebDriverException: Message: Method has not yet been implemented
    
    3. vivo解锁失败，屏幕亮，不解锁
        a. 取消各种锁屏密码，确保手机能滑动解锁
        b. 修改滑动解锁参数, up=上滑/down=下滑/left=左滑/right=右滑
        c. 某些rom默认禁止usb模拟触摸:在开发者选项中允许模拟usb触摸
    
    4. 安装弹窗问题的更优解：
    https://testerhome.com/topics/2601
    

#### 6. 持续优化
    1. 测试结果报告截图
    2. 扩展关键字驱动（优先级低）

    3. # htmltestrunner多进程优化
```
    Exception in thread Thread-4:
    Traceback (most recent call last):
      File "C:\Program Files\Python36\lib\threading.py", line 916, in _bootstrap_inner
        self.run()
      File "C:\Program Files\Python36\lib\threading.py", line 864, in run
        self._target(*self._args, **self._kwargs)
      File "C:\Program Files\Python36\lib\unittest\runner.py", line 176, in run
        test(result)
      File "C:\Program Files\Python36\lib\unittest\suite.py", line 84, in __call__
        return self.run(*args, **kwds)
      File "C:\Program Files\Python36\lib\unittest\suite.py", line 122, in run
        test(result)
    TypeError: 'NoneType' object is not callable

    b. 多线程结果会保存在一个html里面
    目前卡在htmltestrunner746行
 ```
 
    4. 每个页面需要优先判断是否存在异常的toast提示
    
    5. 每个页面被挤下线的优先判断
    
    6. 有些时候appium不能启动app导致报错
    
    7. 我的页面加载缓慢，有时候可能获取到-的问题 
    
    8. 首页登录失败的问题 -> app启动异常 -> 1. 归为错误类用例; 2. 延长自动锁屏时间到10分钟