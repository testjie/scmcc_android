'''
首页-业务办理
'''

# -*- coding: utf-8  -*-
__author__ = 'snake'

import unittest, time
from src.util.util_logger import logger
from src.case.v350.test_basecase import TestCaseBase


class TestYwbl(TestCaseBase):

    @unittest.skip("")
    def test_ywbl_jiasubao(self):
        """
        办理不限量加速包
        :return:
        """
        # 进入业务办理页面
        yewubanli_index = self.find_element("xpath", "//android.widget.TextView[@text='业务办理']")
        yewubanli_index.click()
        # 进入加速包办理页面
        jiasubao_index = self.find_element("xpath", "//android.widget.TextView[@text='加速包']")
        jiasubao_index.click()

        # 办理1G 5元/天加速包
        choice_index = self.find_element("xpath",
                                    "//android.widget.GridView/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]")
        choice_index.click()

        ensure = self.find_element("xpath",
                              "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/bt_todo_business']")
        ensure.click()

        twice_ensure = self.find_element("xpath",
                                    "//android.widget.Button[@resource-id='com.sunrise.scmbhc:id/confirmButton']")
        twice_ensure.click()

    @unittest.skip("")
    def test_Ywbl_liuliangyuebao(self):
        '''
        办理流量月包
        :return:
        '''
        # 进入业务办理页面
        yewubanli_index = self.find_element("xpath", "//android.widget.TextView[@text='业务办理']")
        yewubanli_index.click()

        # 进入办理流量月包主页面
        llyb_index = self.find_element("xpath", "//android.widget.TextView[@text='流量月包']")
        llyb_index.click()
        time.sleep(2)

        # 办理500M 20元/月或办理30G 150元/月
        banli500M20yuan = self.find_element("xpath", "//android.widget.TextView[@text='20元/月']", timeout=5)
        if banli500M20yuan is None:
            logger.info("未找到500M 20元/月")
            self.assertEquals(1, 2, "500M 20元/月办理失败")
            time.sleep(1)
            self.driver.back()
        else:
            logger.info("找到500M 20元/月")
            banli500M20yuan.click()

            ensure = self.find_element("xpath",
                                  "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/bt_todo_business']",
                                  timeout=5)
            ensure.click()

            twice_ensure = self.find_element("xpath",
                                        "//android.widget.Button[@resource-id='com.sunrise.scmbhc:id/confirmButton']")
            twice_ensure.click()

            tishiyu = self.find_element("id", "com.sunrise.scmbhc:id/message")
            self.assertIsNone(tishiyu, "未找到提示语信息")
            if tishiyu.text == " ":
                self.assertEquals(1, 2, "流量月包办理异常")
            else:
                self.assertEquals(1, 1, "500M 20元/月流量月包办理失败或办理成功")

    @unittest.skip("")
    def test_Ywbl_liuliangxiaoshibao3元3小时3G(self):
        '''
        办理流量小时包3G 3元/3小时
        :return:
        '''
        # 进入业务办理页面
        yewubanli_index = self.find_element("xpath", "//android.widget.TextView[@text='业务办理']")
        yewubanli_index.click()

        # 进入流量小时包/日包
        liuliangxiaoshibao = self.find_element("xpath", "//android.widget.TextView[@text='流量小时/日包']")
        liuliangxiaoshibao.click()

        # 办理3G 3元/3小时
        banli_index = self.find_element("xpath", "//android.widget.TextView[@text='3元/3小时']")

        banli_index.click()

        # 点击确认按钮
        ensure = self.find_element("xpath",
                              "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/bt_todo_business']")
        ensure.click()

        # 点击二次确认按钮
        twice_ensure = self.find_element("xpath",
                                    "//android.widget.Button[@resource-id='com.sunrise.scmbhc:id/confirmButton']")
        twice_ensure.click()

        tishiyu = self.find_element("xpath",
                               "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/message00']")
        self.assertIsNone(tishiyu, "未找到提示语信息")
        if tishiyu.text == "":
            self.assertTrue(False, "流量小时包办理异常")
        else:
            self.assertTrue(True, "流量小时包办理成功或者失败")

    def test_Ywbl_yiyueliuliangbao(self):
        '''
        办理音乐流量包
        :return:
        '''

        # 进入业务办理页面
        yewubanli_index = self.find_element("xpath", "//android.widget.TextView[@text='业务办理']")
        yewubanli_index.click()

        # 进入音乐流量包
        # yinyue_index = self.find_element("xpath", "//android.widget.TextView[@text='音乐流量包']")
        service_expandable_list = self.find_element("id", "com.sunrise.scmbhc:id/service_expandable_list")
        yinyue_index = service_expandable_list.find_element("xpath", "//android.widget.TextView[@text='音乐流量包']")
        yinyue_index.click()

        # 选择网易云音乐1G
        wangyi1G = self.find_element("xpath", "//android.widget.TextView[@text='网易云音乐_1G']")
        wangyi1G.click()

        # 点击确认按钮
        ensure = self.find_element("xpath",
                              "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/bt_todo_business']")
        ensure.click()

        # 二次确认办理
        twice_ensure = self.find_element("xpath",
                                    "//android.widget.Button[@resource-id='com.sunrise.scmbhc:id/confirmButton']")
        twice_ensure.click()

        tishiyu = self.find_element("xpath",
                               "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/message00']")
        self.assertIsNotNone(tishiyu, "办理失败")

        if tishiyu.text == "办理失败！":
            logger.info("业务办理失败")
            self.assertTrue(True, "业务办理失败，")
        else:
            self.assertTrue(False, "业务办理异常")

    def test_Ywbl_spllb(self):
        '''
        办理爱奇艺_6G视频流量包
        :return:
        '''
        # 进入业务办理页面
        yewubanli_index = self.find_element("xpath", "//android.widget.TextView[@text='业务办理']")
        yewubanli_index.click()

        shipin_index = self.find_element("xpath", "//android.widget.TextView[@text='视频流量包']")
        shipin_index.click()

        # 选择爱奇艺_6G
        aiqiyi6G = self.find_element("xpath", "//android.widget.TextView[@text='爱奇艺_6G']")
        aiqiyi6G.click()

        # 点击确认按钮
        ensure = self.find_element("xpath",
                              "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/bt_todo_business']")
        ensure.click()

        # 二次确认办理
        twice_ensure = self.find_element("xpath",
                                    "//android.widget.Button[@resource-id='com.sunrise.scmbhc:id/confirmButton']")
        twice_ensure.click()
        time.sleep(2)

        tishiyu = self.find_element("xpath",
                               "//android.widget.TextView[@resource-id='com.sunrise.scmbhc:id/message00']")
        if tishiyu.text == "办理失败！":
            logger.info("业务办理失败")
            self.assertTrue(True, "业务办理失败，")
        else:
            self.assertTrue(False, "业务办理异常")
