# -*- coding: utf-8 -*-
__author__ = 'snake'

import unittest
from src.util.util_logger import logger
from src.util.util_appium_driver import get_driver


@unittest.skip("Skipping this class")
class TestCaseLogout(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("="*100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("="*100)

    def setUp(self):
        logger.info("*"*100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def tearDown(self):
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def test_case4(self):
        print("I'm test_case1")
        assert self.driver is None

    def test_case5(self):
        assert self.driver is not None

    def test_case6(self):
        assert self.driver is not None
