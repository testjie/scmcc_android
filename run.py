# -*- coding: utf-8 -*-
__author__ = 'snake'

import os
import time
import unittest
from multiprocessing import Process

from src.util.util_email import Mail
from src.util.util_logger import logger
from src.util.util_excel import read_excel
from src.util.util_xml import get_phone_config
from src.util.util_xml import get_project_config
from src.util.util_adb import is_connect_devices
from src.util.util_adb import restart_adb_server
from src.util.util_email import EmailReportTemplate
from src.util.util_appium_server import AppiumServer
from src.util.util_htmltestrunner import HTMLTestRunner
from src.util.util_htmltestrunner_debug import HTMLTestRunner
from src.util.util_appium_server import is_appium_server_running


def run(ver):
    """
    :param ver:版本信息，格式见project.xml
    :return:
    """
    # 重启adb-server 防止adb for windows抽风
    logger.info("正在重启adb-server,等待10秒")
    restart_adb_server()
    time.sleep(10)
    version_info = ver.get("version").replace(".", "").lower()

    # 判断哪些设备已经连接
    devices = is_connect_devices(get_phone_config())
    if len(devices) == 0 or len(devices) > 1:
        logger.error("未发现已连接的手机或手机数量大于1，终止本次测试")
        return False

    # 多线程运行appium-server
    _start_appium_servers(devices)
    if is_appium_server_running() is False:
        logger.error("appium-server启动失败,终止本次测试")
        return False

    # 创建测试报告文件夹
    reports = "./reports/"
    if not os.path.exists(reports):
        os.makedirs(reports)

    # 执行case
    start_dir = "./src/case/{}/".format(version_info)
    start_testing_time = time.strftime('%Y-%m-%d-%H-%M')
    title, description = "四川移动掌厅自动测试报告", "version：{} 执行人：欣网四川区测试组".format(version_info)
    suite = unittest.defaultTestLoader.discover(start_dir=start_dir, pattern="test*.py")
    with open("{}/{}-测试报告.html".format(reports, start_testing_time), "wb") as f:
        runner = HTMLTestRunner(stream=f, verbosity=2, title=title, description=description, retry=1)
        result = runner.run(suite)

    # 收集测试报告并反馈
    logger.info("测试用例执行完成,开始收集测试结果")
    success, errors, failures = _generate_results(runner, result, version_info)  # 获取结果，并返回给相关人员
    success, errors, failures = _remove_retry_pass_results(success, errors, failures)   # 错误结果去重

    logger.info("测试结果收集完成，开始发送测试报告")
    _push_results(success, errors, failures, start_testing_time, version_info)  # 发送邮件
    logger.info("关闭appium-server")
    os.system("taskkill /f /im node.exe")  # 关闭appium-server


def _remove_retry_pass_results(success, errors, failures):
    """
    错误结果去重
    :param success:
    :param errors:
    :param failures:
    :return:
    """
    logger.info("开始错误结果去重")
    if len(errors) == 0 and len(failures) == 0:
        logger.info("没有发现失败和错误的case!")
        return success, errors, failures

    # 如果重新retry后的case在success中存在，那么则认为case通过
    for e in errors:
        for s in success:
            if e[2] == s[2]:
                errors.remove(e)

    # 如果重新retry后的case在success中存在，那么则认为case通过
    for f in failures:
        for s in success:
            if f[2] == s[2]:
                failures.remove(f)

    # 失败结果去重
    for f in failures:
        if f[2]._testMethodDoc is not None and "retry" in f[2]._testMethodDoc:
            failures.remove(f)

    # 错误结果去重
    for e in errors:
        if e[2]._testMethodDoc is not None and "retry" in e[2]._testMethodDoc:
            errors.remove(e)

    return success, errors, failures


def _start_appium_servers(devices, ap=4721, bp=4722, sp=4723):
    """
    线程
    :param devices:
    :param ap:  appium-port
    :param bp:  bootstrap-port
    :param sp:  selendroid-port
    :return:
    """
    for _ in devices:
        ap, bp, sp = ap + 3, bp + 3, sp + 3
        server = AppiumServer(ap=ap, bp=bp, sp=sp)
        p = Process(target=server.start_server)
        p.start()


def _generate_results(runner, result, version):
    """
    生成测试测试报告的邮件结果
    :param runner:
    :param result:
    :return:
    """
    s_id, f_id, e_id = 1, 1, 1
    success, errors, failures = [], [], []
    sorted_result = runner.sortResult(result.result)
    cases = read_excel("./doc/掌厅自动化测试用例v1.1.xlsx", version)
    for cid, (cls, cls_results) in enumerate(sorted_result):
        for tid, (n, t, o, e) in enumerate(cls_results):
            module = _get_case_module(cases, t._testMethodName)
            if n == 0:  # success
                res = (s_id, module, t, "成功")
                success.append(res)
                s_id += 1
            if n == 1:  # fail
                res = (f_id, module, t, "失败")
                failures.append(res)
                f_id += 1
            if n == 2:  # error
                res = (e_id, module, t, "错误")
                errors.append(res)
                e_id += 1

    return success, errors, failures


def _push_results(success, errors, failures, start_testing_time, version):
    """
    反馈测试报告
    :param success:
    :param errors:
    :param failures:
    :return:
    """
    # errors反馈给测试
    receiver = ["zhaoyingjie@xwtec.cn", "zhanghaichuan@xwtec.cn"]  # 收件人
    if errors:
        logger.info("发现错误的case:{}".format(errors))
        logger.info("发送错误的case的测试报告邮件，请相及时修改bug")

        # 邮件帐号密码/接受方密码
        report_title = "【错误反馈】 掌厅{}".format(start_testing_time)
        report_desc = EmailReportTemplate().set_content(errors)

        # 发送测试结果
        try:
            _send_mail(receiver, report_title, report_desc)
        except Exception as e:
            logger.error("{}".format(e))

    # failures反馈给所有人
    if failures:
        logger.info("发现执行失败case: --> {}".format(failures))
        logger.info("发送执行失败case的测试报告邮件")

        # 邮件帐号密码/接受方密码
        #  todo 这里需要添加收件人信息
        receiver.append("656882274@qq.com")
        receiver.append("jiangweijun@xwtec.cn")
        receiver.append("tangqiangsheng@xwtec.cn")
        report_title = "【异常告警】四川移动掌厅自动化测试反馈{}-{}".format(version, start_testing_time)
        report_desc = EmailReportTemplate().set_content(failures)
        # 发送测试结果
        try:
            _send_mail(receiver, report_title, report_desc)
        except Exception as e:
            logger.error("{}".format(e))


def _send_mail(receiver, report_title, report_desc):
    """
    发送邮件
    :param receiver:        收件人
    :param report_title:    标题
    :param report_desc:     内容
    :return:
    """
    try:
        mail = Mail("656882274@qq.com", "qsumwxnxynttbbhj", "smtp.qq.com")
        mail.send_mail(receiver, report_title, report_desc)
    except Exception as e:
        logger.error("{}".format(e))


def _get_case_module(cases, case):
    """
    获取case名字
    :param case:
    :return:
    """
    try:
        for cass in cases:
            if cass[4].strip().lower() == case.strip().lower():
                return cass[2].strip() + "-" + cass[1].strip()
    except Exception as e:
        logger.info(e)

    return ""


if __name__ == "__main__":
    """
    run每一个version
    """
    for ver in get_project_config():
        run(ver)
