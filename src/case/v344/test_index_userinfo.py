"""
    首页-手机号-用户信息
"""
# -*- coding: utf-8  -*-
__author__ = 'snake'


import unittest
from src.util.util_logger import logger
from src.bll.v344.login import one_key_login
from src.bll.v344.index import back_to_index
from src.util.util_appium_driver import get_driver
from src.util.util_appium_tools import find_element


class TestCaseUserInfo(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("="*100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("="*100)

    def setUp(self):
        logger.info("*"*100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))
        logger.info("开始执行返回首页")
        back_to_index(self.driver)

        if one_key_login(self.driver) is False:
            logger.info("判断掌厅登录状态失败，app启动异常!")
            raise Exception("判断掌厅登录状态失败，app启动异常!!")

    def tearDown(self):
        self.driver._switch_to.context(self.driver.contexts[0])
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))


    def test_user_name(self):
        """
        检查用户姓名: 姓名不为空且包含'*'号
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        user_name = find_element(self.driver, "id", "com.sunrise.scmbhc:id/user_name", timeout=30).text
        self.assertIn("*", user_name, "判断姓名是否存在*")

    def test_user_state(self):
        """
        检查用户号码状态，检查是否存在'号码状态：正常'关键字
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        user_state = find_element(self.driver, "id", "com.sunrise.scmbhc:id/user_state", timeout=30).text
        self.assertIn("号码状态：正常", user_state, "判断姓名是否存在号码状态关键字")

    def test_user_phone(self):
        """
        检查用户号码，号码与首页号码一致
        :return:
        """
        user_index_num = find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv", timeout=30)
        user_index_phone = user_index_num.text
        user_index_num.click()

        user_info_phone = find_element(self.driver, "id", "com.sunrise.scmbhc:id/user_phone", timeout=30).text
        self.assertEquals(user_index_phone, user_info_phone, "检查用户号码与首页号码一致")

    def test_user_area(self):
        """
        检查用户归属地：当前号卡归属地是成都：'号码归属：成都'
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        user_phone_area = find_element(self.driver, "id", "com.sunrise.scmbhc:id/user_area", timeout=30).text
        self.assertIn(user_phone_area, "号码归属：成都", "检查用户号码归属地为成都")

    def test_user_star(self):
        """
        检查用户星级：不为空/如果是无星级，则是不显示星级
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        user_star_level = find_element(self.driver, "xpath", "//android.widget.TextView[@text='无星级']", timeout=30)
        if user_star_level is None:
            user_start = find_element(self.driver, "id", "com.sunrise.scmbhc:id/id_my_mobile_txt_show", timeout=30).text
            self.assertNotEqual(user_start, None, "检查用户星级不为空, text={}".format(user_start))
        else:
            self.assertTrue(True, "用户星级为无星级")

    def test_user_star_score(self):
        """
        检查用户星级分：不为空
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        star_xpath = "//android.widget.TextView[@text='星级分：']/following-sibling::android.widget.TextView"
        user_start = find_element(self.driver, "xpath", star_xpath, timeout=30).text
        self.assertNotEqual(user_start, None, "检查用户星级分不为空")

    def test_user_star_expiry_date(self):
        """
        检查用户星级有效期：不为空
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        star_xpath = "//android.widget.TextView[@text='星级有效期：']/following-sibling::android.widget.TextView"
        user_star_expiry_date = find_element(self.driver, "xpath", star_xpath, timeout=30).text
        self.assertNotEqual(user_star_expiry_date, None, "检查用户星级有效期不为空")

    def test_user_certification(self):
        """
        检查用户实名认证：不为空
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        certifi_xpath = "//android.widget.TextView[@text='实名认证：']/following-sibling::android.widget.TextView"
        user_certification = find_element(self.driver, "xpath", certifi_xpath, timeout=30).text
        self.assertNotEqual(user_certification, None, "检查用户实名认证不为空")

    def test_user_net_years(self):
        """
        检查用户网龄：不为空
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        year_xpath = "//android.widget.TextView[@text='网龄：']/following-sibling::android.widget.TextView"
        user_net_years= find_element(self.driver, "xpath", year_xpath, timeout=30).text
        self.assertNotEqual(user_net_years, None, "检查用户网龄不为空")

    def test_user_use_years(self):
        """
        检查入网时间：不为空
        :return:
        """
        find_element(self.driver, "id", "com.sunrise.scmbhc:id/head_num_tv").click()
        user_year_xpath = "//android.widget.TextView[@text='入网时间：']/following-sibling::android.widget.TextView"
        user_use_net_years = find_element(self.driver, "xpath", user_year_xpath, timeout=30).text
        self.assertNotEqual(user_use_net_years, None, "检查用户入网时间不为空")
