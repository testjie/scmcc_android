"""
    首页-一键查询-可用积分-积分兑换
"""

# -*- coding: utf-8  -*-
__author__ = 'snake'


import unittest
from src.bll.v344.login import one_key_login
from src.util.util_logger import logger
from src.bll.v344.index import back_to_index
from src.util.util_appium_driver import get_driver
from src.util.util_appium_tools import find_element


@unittest.skip("")
class TestCaseMy(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("="*100)
        logger.info("开始执行测试集->{}".format(cls.__name__))
        logger.info("开始获取driver")
        cls.driver = get_driver()
        logger.info("成功获取driver, driver信息->{}".format(cls.driver))

    @classmethod
    def tearDownClass(cls):
        logger.info("开始关闭driver")
        cls.driver.quit()
        logger.info("成功关闭driver")
        logger.info("="*100)

    def setUp(self):
        logger.info("*"*100)
        logger.info("开始执行用例->{}.{}".format(self.__class__, self._testMethodName))
        logger.info("开始执行返回首页")
        back_to_index(self.driver)

        if one_key_login(self.driver) is False:
            logger.info("判断掌厅登录状态失败，app启动异常!")
            raise Exception("判断掌厅登录状态失败，app启动异常!!")

    def tearDown(self):
        self.driver._switch_to.context(self.driver.contexts[0])
        logger.info("结束执行用例->{}.{}".format(self.__class__, self._testMethodName))

    def test_yjcx_kyjf(self):
        """
        可用积分：积分不能为空
        :return:
        """
        yjcx_path = "//android.widget.TextView[@text='一键查询']"
        find_element(self.driver, "xpath", yjcx_path).click()

        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        score_xpath = "/html/body/div[1]/div[1]/div[2]/p"
        score_obj = find_element(self.driver, "xpath", score_xpath)
        self.assertNotEqual(score_obj.text, None, "可用积分不为空")

    def test_yjcx_xfcx(self):
        find_element(self.driver, "xpath", "//android.widget.TextView[@text='一键查询']").click()
        context = self.driver.contexts[-1]
        logger.info("context is ->{}".format(context))
        self.driver._switch_to.context(context)

        user_star_level = find_element(self.driver, "xpath", "/html/body/div[1]/div[1]/div[1]/a")
        logger.info("user_star_level ->{}".format(user_star_level))
